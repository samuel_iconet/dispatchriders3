<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactionrecords', function (Blueprint $table) {
            $table->id();
            $table->integer('paymentrecord_id')->nullable();
            $table->string('source');
            $table->integer('source_id');
            $table->string('destination');
            $table->integer('destination_id');
            $table->string('comment')->nullable();
            $table->integer('amount');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactionrecords');
    }
}
