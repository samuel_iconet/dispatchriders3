<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');


Route::get('/', 'homecontroller@home');
Route::get('/rider/{company_id}', 'homecontroller@company_detrails');
Route::get('/company_register', function () {
    return view('registercompany');
});
Route::get('/404', function () {
    return view('notfound');
});
Route::get('/companydetails', function () {
    return view('companydetails');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/about', function () {
    return view('aboutus');
});
Route::get('/contact', function () {
    return view('contactus');
});
Route::get('/alldispatch', function () {
    return view('alldispatch');
});
Route::get('confirm/{user_id}/{remember}' , 'authcontroller@confirm');
Route::get('{path}',"homeController@index")->where( 'path', '([A-z\d\-\/_.]+)?' );