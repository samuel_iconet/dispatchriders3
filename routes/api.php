<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// campanies with ranking ranking subscribtion

Route::get('/getcompanies', 'companysubscribtioncontroller@getcompanies');
Route::get('/unsubscribe/{company_id}', 'companysubscribtioncontroller@unsubscribeFrom');
Route::get('/subscribe/{company_id}', 'companysubscribtioncontroller@subscribeto');
 // get rider request details
 Route::get('/sub_companies/getdetails', 'riderrequestcontroller@getdetails');

//loginin attempt
Route::get('/attemptlogin', 'usercontroller@attemptlogin');
Route::post('/requestquote', 'homecontroller@requestquote');

// user management
Route::post('/user/changepassword', 'usermanagementcontroller@editpassword');
Route::post('/user/edituser', 'usermanagementcontroller@editUser');
 
// user support ticket
Route::get('/ticket/user', 'supportticketcontroller@gettickets');
Route::post('/ticket/create', 'supportticketcontroller@createticket');
Route::get('/ticket/{ticket_id}', 'supportticketcontroller@getticket');
Route::post('/ticket/reply', 'supportticketcontroller@replyticket');
Route::get('/ticket/endticket/{ticket_id}', 'supportticketcontroller@endticket');


// auth routes

Route::post('/register', 'authcontroller@register');
Route::post('/company_register', 'authcontroller@company_register');
Route::post('/login', 'authcontroller@login');
Route::post('/subscribe', 'authcontroller@subscribe');

//user dashboard 
Route::get('/user/home', 'userdasboardcontroller@home');
Route::get('/request/cancel/{request_id}', 'userdasboardcontroller@cancelRequest');
Route::post('/request/confirm', 'userdasboardcontroller@confirmRequest');

//user account manager

Route::get('/user/account', 'useraccountcontroller@getAccount');
