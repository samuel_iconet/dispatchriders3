// @ts-nocheck
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue/dist/vue.common.js';
window.Vue = Vue;
import VueSweetalert2 from 'vue-sweetalert2';
 

Vue.use(VueSweetalert2);

 
Vue.use(VueSweetalert2);

import VueRouter from 'vue-router';
Vue.use(VueRouter);
Vue.use(require('vue-chartist'));
Vue.use(require('chartist-plugin-tooltips-updated'));
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'

Vue.use(VueMaterial);

let Fire = new Vue();
window.Fire = Fire;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

let routes = [
  { path: '/dashboard', component: require('./components/dashboardprep.vue').default },
  { path: '/userdashboard', component: require('./components/userdashboard.vue').default },
  { path: '/account', component: require('./components/account.vue').default },
  { path: '/support', component: require('./components/support.vue').default },
  { path: '/companylist', component: require('./components/companylist.vue').default },
  { path: '/chat', component: require('./components/chat.vue').default },
    { path: "*",component: require('./components/notfound.vue').default }
    
   ]
   
   const router = new VueRouter({
       mode: 'history',
     routes // short for `routes: routes`
   })
   
   const app = new Vue({
   
       el: '#app',
       router
   });
   