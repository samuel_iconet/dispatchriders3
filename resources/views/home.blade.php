@extends('layouts.homelayout')
@section('body')
<header class="page-header page-header-dark bg-img-cover overlay" style='background-image: url("assets/img/bike.jpg")'>
    <div class="page-header-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-10 text-center">
                    <h1 class="page-header-title">Dispatch Riders</h1>
                    <p class="page-header-text mb-8">Dispatch riders has a collection of the best logistics company, ready to deliver your item at a speed of light <img style="width: 60px ; height: 60px;" src="/speed.jpg" alt=""></p>
                    <form class="page-header-signup">
                        <div class="form-row justify-content-center">
                            <div class="col-lg-6 col-md-8">
                                <div class="form-group mr-0 mr-lg-2"><label class="sr-only" for="inputSearch">Find A Logistic Compay...</label><input class="form-control form-control-solid rounded-pill" id="inputSearch" type="text" placeholder="Find A Logistic Company..." /></div>
                            </div>
                            <div class="col-lg-3 col-md-4"><button class="btn btn-teal btn-block btn-marketing rounded-pill" type="submit">Search</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-angled text-light">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>

<section class="bg-light py-10">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="mb-5 text-center">
                    <div class="text-xs text-uppercase-expanded text-primary mb-9"><p style="font-size: 30px">Our Logistics Companies</p></div>
                    <p class="lead mb-0">Get your items delivered fast and at very affordable rates with our most reliable and trusted delivery services around you.</p>
                </div>
            </div>
        </div><hr><hr><hr>
        <div class="row justify-content-center" style="  background-image: linear-gradient(to right, #7B1FA2, #E91E63)">
            @if(isset($companies))
            @foreach($companies as $company)
            <div class="col-md-6 col-lg-4 col-xl-3 mb-5">
            <a class="card lift" href="/rider/{{$company->id}}"
                    ><img class="card-img-top" src="/assets/img/bike.jpg" alt="..." />
            <div class="card-body text-center py-3"><h6 class="card-title mb-0">{{$company->name}}</h6>
                    
                            <span class="heading"> Rating</span>
                            @for($i = 0 ; $i < $company->rating_rounded ; $i++)
                            <span class="fa fa-star checked"></span>
                            @endfor
                            @if($company->rating != $company->rating_rounded)
                            <?php $val = 1 ?>
                            <span class="fa fa-star-half-alt checked" ></span>
                            @else
                            <?php $val = 0 ?>
                            @endif
                            @for($i = 0 ; $i < 5- $company->rating_rounded - $val ; $i++)
                            <span class="fa fa-star "></span>
                            @endfor
                            <p> <span class="badge badge-warning">{{$company->rating}}</span>  average based on {{count($company->reviews)}} reviews.</p>
                                </div></a>
                    </div>
                
            @endforeach
            @else

            @endif
                             </div>
        <div class="row" style="padding: 80px;"> <div class="col-lg-2 offset-5"><a href="/alldispatch" class="btn btn-success ">View All</a ></div></div>

    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</section>
<header class="page-header page-header-dark bg-img-repeat bg-secondary" style='background-image: url("assets/img/pattern-shapes.png")'>
    <div class="page-header-content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="badge badge-marketing badge-pill badge-secondary-soft text-secondary mb-3">Start today!</div>
                    <h1 class="page-header-title">Register Now</h1>
                    <p class="page-header-text">Get your package delivered to you at the best price and convinience</p>
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Sign up now</div>
                        <div class="card-body">
                            <h3 class="alert alert-success" id="message"  style="display: none"></h3>
                            <h3 class="alert alert-danger" id="error"  style="display: none"></h3>

                            <form id="target" action="/sum">
                                <div class="form-row">
                                    <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapFirstName">First Name</label><input required  class="form-control rounded-pill" id="fname" type="text" /></div>
                                    <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapLastName">Last Name</label><input required  class="form-control rounded-pill" id="lname" type="text" /></div>
                                </div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email address</label><input required  class="form-control rounded-pill" id="email" type="email" /></div>
                                <div class="form-group">
                                    <label class="small text-gray-600" for="leadCapEmail">Password</label><input required minlength="6" class="form-control rounded-pill" id="password" type="password" />
                                </div>
                                <button type="submit" id="submit"  class="btn btn-primary btn-marketing btn-block rounded-pill mt-4"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Sign Up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>
<section class="bg-white pt-5 pb-10">
    <div class="container">
        <div class="card mt-n15 mb-10 z-1">
            <div class="card-body p-5">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <h4>Get the latest news</h4>
                        <p class="lead text-gray-500 mb-0">Stay in the loop with the latest updates and features added to our application!</p>
                        <h3 class="alert alert-success" id="message1"  style="display: none"></h3>
                        <h3 class="alert alert-danger" id="err1"  style="display: none"></h3>

                    </div>
                    <div class="col-lg-6">
                        <form id="subscribe">

                        <div class="input-group mb-2">
                            <input class="form-control form-control-solid" type="email" required  placeholder="youremail@example.com" id="subemail" aria-label="Recipient's username" aria-describedby="button-addon2" />
                            <div class="input-group-append"><button class="btn btn-primary" type="submit" id="submit1"  ><i id="loader1" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Sign up</button></div>
                            
                        </div>
                    </form> 

                        <div class="small text-gray-500">You can unsubscribe at any time.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="mb-5 text-center">
                    <div class="text-xs text-uppercase-expanded text-primary mb-2">New Businesses</div>
                    <p class="lead mb-0">Here are some of the most latest Delivery companies to join us.</p>
                </div>
            </div>
        </div>
        <div class="row">
           
            @if(isset($latestcompanies))
            @foreach($latestcompanies as $company)
            <div class="col-lg-4 mb-5 mb-lg-0">
                <a class="card lift" href="/rider/{{$company->id}}"
                    ><img class="card-img-top" src="/assets/img/bike.jpg" alt="..." />
                    <div class="card-body text-center py-3">
                        <h6 class="card-title mb-0">{{$company->name}}</h6>
                        @for($i = 0 ; $i < $company->rating_rounded ; $i++)
                        <span class="fa fa-star checked"></span>
                        @endfor
                        @if($company->rating != $company->rating_rounded)
                        <?php $val = 1 ?>
                        <span class="fa fa-star-half-alt checked" ></span>
                        @else
                        <?php $val = 0 ?>
                        @endif
                        @for($i = 0 ; $i < 5- $company->rating_rounded - $val ; $i++)
                        <span class="fa fa-star "></span>
                        @endfor
                          <div class="small mb-2">({{count($company->reviews)}} Reviews)</div>
                        <div class="small">{{$company->address}}</div>
                    </div>
                    <div class="card-footer text-center text-xs"><i class="fas fa-store-alt mr-1"></i>Joined {{$company->created_at->diffForHumans()
                    }}</div></a
                >
            </div>
                
            @endforeach
            @else

            @endif
          
        </div>
    </div>
    <div class="svg-border-angled text-dark">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</section>
<section class="bg-white py-10">
    <div class="container">
        <div class="row mb-10">
            @foreach($reviews as $review)
            <div class="col-lg-6 mb-5 mb-lg-0 divider-right">
                <div class="testimonial p-lg-5">
                    {{-- <div class="avatar avatar-xl mb-3"><img class="avatar-img" src="https://source.unsplash.com/QAB-WJcbgJk/96x96" /></div> --}}
                <p class="testimonial-quote text-primary">{{$review->review}}</p>
                <div class="testimonial-name">{{$review->name}}</div>
                <div class="testimonial-position">{{$review->title}}</div>
                </div>
            </div>
           @endforeach
        </div>
      
    </div>
</section>
@endsection
@section('script')

<script type="text/javascript">

     $( document ).ready(function() {
     
        $( "#target" ).submit(function( event ) {
       
        event.preventDefault();
        $('#loader').show();
        $('#submit').attr('disabled','disabled');
        let email = $('#email').val();
        let username =  $('#fname').val() + " " + $('#lname').val();
        let password = $('#password').val();
    
        $.ajaxSetup({
                headers: { }
            });
$.post('/api/register',   // url
       {        username: username, 
                password: password,
                email: email,
                role: 3
                
       }, 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            $('#loader').hide();
           $('#submit').removeAttr('disabled');
           $('#message').show(); 
           $('#message').text("Registration Successfull please Login to your Mail to complete the process." );
        }else{
            $('#error').show(); 
           $('#error').text(data.email[0]);
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
        }


        }).fail(function(jqxhr, settings, ex) {
           $('#err').show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
           alert("An Error Occured on the Server.")
         });




        });
$( "#subscribe" ).submit(function( event ) {
       
       event.preventDefault();
       $('#loader2').show();
       $('#submit2').attr('disabled','disabled');
       let email = $('#subemail').val();
      
   
       $.ajaxSetup({
               headers: { }
           });
$.post('/api/subscribe',   // url
      {       
               email: email
               
      }, 
      function(data, status, jqXHR) {// success callback

       console.log(status);
       console.log(data);      
             
       if(data.code == "200"){
           $('#loader1').hide();
           $('#err1').hide();
          $('#submit1').removeAttr('disabled');
          $('#message1').show(); 
          $('#message1').text("Your Subscrition to Dispatch Riders was successful." );
       }else{
          $('#message1').hide();
           $('#err1').show(); 
          $('#err1').text("Email Already Exist");
          $('#loader1').hide();
          $('#submit1').removeAttr('disabled');
       }


       }).fail(function(jqxhr, settings, ex) {
          $('#err1').show();
          $('#message1').hide();
          $('#err1').text("Network Error");
          $('#loader1').hide();
          $('#submit1').removeAttr('disabled');
          alert("An Error Occured on the Server.")
        });




       });
     });
   </script>
@endsection