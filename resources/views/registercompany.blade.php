@extends('layouts.homelayout')
@section('body')   
<header class="page-header page-header-dark bg-img-repeat bg-secondary" style='background-image: url("assets/img/bike.jpg")'>
        <div class="page-header-content">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2">
                        
                    </div>
                    <div class="col-lg-8">
                        <div class="card rounded-lg text-dark">
                            <div class="card-header py-4">Register Your Dispatch Company now</div>
                            <div class="card-body">
                                <form id="target" enctype="multipart/form-data">
                                    <hr><h2>Company Details </h2><hr>
                                    
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Name</label><input required name="company_name" class="form-control rounded-pill"  type="text" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Email address</label><input required name="company_email" class="form-control rounded-pill"  type="email" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Phone Number</label><input required name="company_contact" class="form-control rounded-pill"  type="number" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s  Address</label><textarea  name="company_address" class="form-control " cols="30" rows="10"></textarea></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s  Moto/Description</label><textarea  name="company_moto" class="form-control " cols="30" rows="10"></textarea></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s State</label>
                                        <select class="form-control" name="state" id="" v-model="state_id">
                                            <option value="abuja">Abu8ja</option>
                                        </select>
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s  Logo</label> <br><input required name="company_logo" type="file"> <hr></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s  CAC registeration</label> <br><input required name="company_cac" type="file"><hr></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s  Licence</label><br><input required name="company_licence" type="file"><hr> </div>
                                    <hr><h2>Company Bank account Details (settlement Account) </h2><hr>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Bank Name</label><input required name="company_bank"  class="form-control rounded-pill"  type="text" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Account Name</label><input required name="company_accnum" class="form-control rounded-pill"  type="text" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Company`s Account Number</label><input required name="company_accname" class="form-control rounded-pill"  type="number" /></div>

                                    <hr><h2>Company Administrator Details </h2><hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapFirstName">First Name</label><input required name="fname" class="form-control rounded-pill"  type="text" /></div>
                                        <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapLastName">Last Name</label><input required name="lname" class="form-control rounded-pill"  type="text" /></div>
                                    </div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapEmail"> Email address</label><input required name="email" class="form-control rounded-pill"  type="email" /></div>
                                    <div class="form-group"><label class="small text-gray-600" for="leadCapCompany">Password</label><input required name="password" class="form-control rounded-pill" type="password" /></div>
                                    <hr><button class="btn btn-primary btn-marketing btn-block rounded-pill mt-4" type="submit" id="submit" ><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i> Register</button>
                                    <p id="message" style="display: none; color: green" >Your registeration was successful please, verify your Email to conplete Registration.</p>
                                    <p id="error" style="display: none; color: red" ></p>
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="svg-border-angled text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
        </div>
    </header> 
    @endsection
    @section('script')

<script type="text/javascript">

    $( document ).ready(function() {
    
        $(document).ready(function (e) {
 $("#target").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
         url: "/api/company_register",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData: false,
   beforeSend : function()
   {
    $('#loader').show();
    $('#submit').attr('disabled','disabled');
   },
   success: function(data)
      {
   console.log(data);
    $('#loader').hide();
    $('#submit').removeAttr('disabled');
    if(data.code == '200'){
        $('#message').show(); 
        $('#error').hide(); 

    }else if(data.code == '404'){
        $('#message').hide(); 
        $('#error').show(); 
           $('#error').text("Company Email Already Used");
    }else{
        $('#message').hide(); 
        $('#error').show(); 
           $('#error').text("User Email Already Used");
    
    }
      },
     error: function(e) 
      {
        $('#loader').hide();
        $('#submit').removeAttr('disabled');
      console.log(e);
      $('#message').hide(); 
        $('#error').show(); 
           $('#error').text("Network Error.");
    
      }          
    });
 }));
});   
 
 });
   </script>

@endsection