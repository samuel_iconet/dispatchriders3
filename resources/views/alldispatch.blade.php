@extends('layouts.homelayout')
@section('body')
<header class="page-header page-header-dark ">
    <div class="page-header-content ">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <h1 class="page-header-title mb-3">Dispatch List</h1>
                    <div class="form-group">
                        <label for="sel1">Select State:</label>
                        <select class="form-control" id="sel1">
                          <option>AbuJa FCT</option>
                          <option>Plateau State</option>
                          <option>Rivers State</option>
                          <option>Lagos</option>
                        </select>
                      </div>
                </div>
            </div>
        </div>
    </div>
    
</header>

<section class="bg-white py-10">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Outdoor Patio</h4>
                        <p class="card-text">Our property features a beautiful, private outdoor area with seating and a pool.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Full Kitchen</h4>
                        <p class="card-text">A fully stocked kitchen with all modern amenities provides a peaceful cooking environment.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Comfortable Bedding</h4>
                        <p class="card-text">With three newly updated bedrooms you will be sleeping soundly during your stay.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Outdoor Patio</h4>
                        <p class="card-text">Our property features a beautiful, private outdoor area with seating and a pool.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Full Kitchen</h4>
                        <p class="card-text">A fully stocked kitchen with all modern amenities provides a peaceful cooking environment.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
            <div class="col-lg-4 mb-5">
                <a class="card lift h-100" href="#!"
                    ><img class="card-img-top" src="assets/img/bike.jpg" alt="..."/>
                    <div class="card-body">
                        <h4 class="card-title mb-2">Comfortable Bedding</h4>
                        <p class="card-text">With three newly updated bedrooms you will be sleeping soundly during your stay.</p>
                    </div>
                    <div class="card-footer bg-transparent border-top d-flex align-items-center justify-content-between">
                        <div class="small text-primary">See more</div>
                        <div class="small text-primary"><i data-feather="arrow-right"></i></div></div
                ></a>
            </div>
        </div>
       
    </div>
</section>
@endsection