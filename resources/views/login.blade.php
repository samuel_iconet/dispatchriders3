@extends('layouts.homelayout')
@section('body')
<header class="page-header bg-img-repeat ">
    <div class="page-header-content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg text-dark">
                        <div class="card-header py-4">Login now</div>
                    @if (isset($message))
                    <p style="color: green">{{$message}}</p>
                   @endif
                   @if (isset($error))
                   <p style="color: red">{{$error}}</p>
                  @endif
                  <p id="err" style="display: none; color: red" >Invalid Email and Password Provided </p>
                  <p id="err1" style="display: none; color: red" >Your account is not active, please conirm your email. </p>

                        <div class="card-body">
                            <form id="target">
                               
                                <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email address</label><input required class="form-control rounded-pill" id="email"  type="email" /></div>
                                <div class="form-group"><label class="small text-gray-600" for="leadCapCompany">Password</label><input required class="form-control rounded-pill" id="password" type="password" /></div>
                                <button class="btn btn-primary btn-marketing btn-block rounded-pill mt-4"  id="submit" type="submit"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-angled text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100" /></svg>
    </div>
</header>

@endsection
@section('script')

<script type="text/javascript">

//     $( document ).ready(function() {
//      // $( "#err" ).hide();
//       let token = "Bearer "  + localStorage.getItem('access_token');
//       console.log(token);
//      $.ajax({
//      type: "GET",
//      headers: {'Content-Type': 'application/json', Authorization: token  },
//      url: "/api/attemptlogin",
//      success: function(patientDTO) {
//          console.log("SUCCESS: ", patientDTO);
//           window.location.href = '/dashboard';
//           },
//      error: function(e) {
//      console.log("ERROR: ", e);
     
//      }
//  });
$( document ).ready(function() {
  
    $( "#target" ).submit(function(e) {
         e.preventDefault();
      $('#loader').show();
      $('#submit').attr('disabled','disabled');
     let email = $('#email').val();
     let password = $('#password').val();
     let token2 = '';
     console.log(email);
     console.log(password);
 $.ajaxSetup({
                 headers: { }
             });
 $.post('/oauth/token',   // url
        {        username: email, 
                 password: password,
                 grant_type: "password",
                 client_id: "2",
                 client_secret: "A0faLcaxXb3uBDdoWUjNX7LYOorOgmNN4WBJfgxA",
                 scope:"*"
        }, // data to be submit
     // $.post('/api/token',   // url
     //    {        email: email, 
     //             password: password
                
     //    }, // data to be submit
        function(data, status, jqXHR) {// success callback
 
         console.log(status);
                // console.log(data.access_token);
               localStorage.setItem('access_token', data.access_token);
                token2  = data.access_token;
                   console.log(token2);
                  token2 = "Bearer "  + token2;
                   console.log(token2);
                   $.ajax({
                   type: "GET",
                   headers: {'Content-Type': 'application/json', Authorization: token2  },
                   url: "/api/user",
                   success: function(patientDTO) {
                       console.log("SUCCESS: ", patientDTO);
                                  if(patientDTO.status == '0'){
                                    $('#err1').show();
                                    $('#err').hide();
                                    $('#loader').hide();
                       $('#submit').removeAttr('disabled');
                                    }else{
                                localStorage.setItem('name', patientDTO.username);
                                  localStorage.setItem('email', patientDTO.email);
                                  localStorage.setItem('id', patientDTO.id);
                                  localStorage.setItem('role', patientDTO.role);
                                  localStorage.setItem('status', patientDTO.status);
                                  localStorage.setItem('created_at', patientDTO.created_at);
                                //  if(patientDTO.role == '1'){
                                //     window.location.href = '/userdashboard';
                                //  }else if(patientDTO.role == '2'){
                                //     window.location.href = '/admindashboard';
                                //  }else if(patientDTO.role == '3'){
                                //     window.location.href = '/artisandashboard';
                                //  }else{
                                //      console.log("herer")
                                //  }
                                window.location.href = '/dashboard';
                                  }
                        },
                   error: function(e) {
                      $('#err').show();
                       $('#loader').hide();
                       $('#submit').removeAttr('disabled');
                       alert(e);
                   }
               });
 
 
 
         }).fail(function(jqxhr, settings, ex) {
            $('#err').show();
            $('#loader').hide();
            $('#submit').removeAttr('disabled');
            console.log(jqxhr.status);
            if(jqxhr.status == 400){
                alert("Invalid Email and Password Provided");
            }else{
                alert("Network Error");
            }
          });
 
 
 
 
 });
 });
   </script>

@endsection