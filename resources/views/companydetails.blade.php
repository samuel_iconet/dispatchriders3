@extends('layouts.homelayout')
@section('body')
 
<header class="page-header page-header-dark bg-gradient-primary-to-secondary">
    <div class="page-header-content pt-0">
        
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-lg-1">
                 <img class="rounded-circle z-depth-2" style="width: 300px; height: 300px;" alt="100x100" src="/assets/img/bike.jpg"
                data-holder-rendered="true">
                </div>
                <div class="col-lg-8">
                <h1 class="page-header-title mb-3">{{$company->name}}</h1>
                    <p class="page-header-text">{{$company->address}} </p>
                    <p class="page-header-text">{{$company->contact}} </p>
                </div>

            </div>
        </div>
    </div>
    <div class="svg-border-rounded text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor"><path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0" /></svg>
    </div>
</header>
<section >
  <div class="row ">
      <div class="col-lg-8">
        <div class="card rounded-lg text-dark ">
            <div class="card-header py-4">Available Routes And Prices</div>
            <div class="card-body table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-borderless">
                    <thead>
                      <tr class="alert alert-info">
                        <th scope="col">#</th>
                        <th scope="col">FROM</th>
                        <th scope="col">TO</th>
                        <th scope="col">Duration</th>
                        <th scope="col">PRICE</th>
                      </tr>
                    </thead>
                    <tbody >
                        <?php $count = 0 ?>
                        @foreach ($company->routes as $route)
                        <tr>
                            <th scope="row">{{++$count}}</th>
                        <td>{{$route->pickup}}</td>
                            <td>{{$route->destination}}</td>
                            <td style="color: #4CAF50;">{{$route->duration}}</td>
                        <td style="color: #4CAF50;">₦{{$route->price}}</td>
                          </tr>
      
                        @endforeach
                      
                    
                    </tbody>
                  </table>
            </div>
        </div>
      </div>
    <div class="col-lg-4 ">
        <div >
            <div class="card rounded-lg text-dark">
                <div class="card-header py-4">Request A delivery Quote Now</div>
                <p id="message" style="display: none; color: green" >Your quotation request has been sent successfully expect a feedback soon. </p>
                <p id="error" style="display: none; color: red" >Your quotation request was not Successful please try again. </p>

                <div class="card-body">
                    <form id="target">
                        <div class="form-row">
                        <input type="hidden" id="company_id" value="{{$company->id}}">
                            <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapFirstName">From</label><input required  class="form-control rounded-pill" id="pickup" type="text" /></div>
                            <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapLastName">To</label><input required class="form-control rounded-pill" id="destination" type="text" /></div>
                            <div class="form-group col-md-6"><label class="small text-gray-600" for="leadCapLastName">Description</label><textarea required class="form-control rounded-pill" id="describtion" type="text" ></textarea></div>
                        </div>
                        <div class="form-group"><label class="small text-gray-600" for="leadCapEmail">Email address</label><input class="form-control rounded-pill" id="email" type="email" /></div>
                        <button class="btn btn-primary btn-marketing btn-block rounded-pill mt-4" id="submit" type="submit"><i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>Get Quote</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>  
</section>
<div class="container-fluid px-0 py-5 mx-auto">
    <div class="row justify-content-center mx-0 mx-md-auto">
        <div class="col-lg-10 col-md-11 px-1 px-sm-2">
            <div class="card border-0 px-3">
                <!-- top row -->
                <div class="d-flex row py-5 px-5 bg-light">
                    <div class="green-tab p-2 px-3 mx-2">
                        <p class="sm-text mb-0">OVERALL RATING</p>
                        <h4>{{$company->rating}}</h4>
                        @for($i = 0 ; $i < $company->rating_rounded ; $i++)
                        <span class="fa fa-star checked"></span>
                        @endfor
                        @if($company->rating != $company->rating_rounded)
                        <?php $val = 1 ?>
                        <span class="fa fa-star-half-alt checked" ></span>
                        @else
                        <?php $val = 0 ?>
                        @endif
                        @for($i = 0 ; $i < 5- $company->rating_rounded - $val ; $i++)
                        <span class="fa fa-star "></span>
                        @endfor
                    </div>
                    
                    <div class="white-tab p-2 mx-2 text-muted">
                        <p class="sm-text mb-0">ALL REVIEWS</p>
                        <h4>{{count($company->reviews)}}</h4>
                    </div>
                    <div class="white-tab p-2 mx-2">
                        <p class="sm-text mb-0 text-muted">POSITIVE REVIEWS</p>
                    <h4 class="green-text">{{round(($company->rating / 5) * 100)}}%</h4>
                    </div>
                </div> <!-- middle row -->
               <div  class="table-wrapper-scroll-y my-custom-review">
                   @foreach($company->reviews as $review)
                <div class="review p-5">
                    <div class="row d-flex">
                        <div class="profile-pic"><img src="https://i.imgur.com/Mcd6HIg.jpg" width="60px" height="60px"></div>
                        <div class="d-flex flex-column pl-3">
                            <h4>{{$review->user->name}}</h4>
                            <p class="grey-text">{{$review->created_at->diffForHumans()}}</p>
                        </div>
                    </div>
                    <div class="row ">
                        @for($i = 0 ; $i < $review->rating ; $i++)
                        <span class="fa fa-star checked"></span>
                        @endfor
                        @for($i = 0 ; $i < 5- $review->rating; $i++)
                        <span class="fa fa-star "></span>
                        @endfor
                       
                    </div>
                    <div class="row ">
                    <p>{{$review->review}}</p>
                    </div>
                    
                </div>
               @endforeach
               
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
    .my-custom-scrollbar {
    position: relative;
    height: 340px;
    overflow: auto;
    }
    .my-custom-review {
    position: relative;
    height: 300px;
    overflow: auto;
    }
    .table-wrapper-scroll-y {
    display: block;
    }
                body {
        overflow-x: hidden
    }
    
    .container-fluid {
        background-image: linear-gradient(to right, #7B1FA2, #E91E63)
    }
    
    .sm-text {
        font-size: 10px;
        letter-spacing: 1px
    }
    
    .sm-text-1 {
        font-size: 14px
    }
    
    .green-tab {
        background-color: #00C853;
        color: #fff;
        border-radius: 5px;
        padding: 5px 3px 5px 3px
    }
    
    .btn-red {
        background-color: #E64A19;
        color: #fff;
        border-radius: 20px;
        border: none;
        outline: none
    }
    
    .btn-red:hover {
        background-color: #BF360C
    }
    
    .btn-red:focus {
        -moz-box-shadow: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        outline-width: 0
    }
    
    .round-icon {
        font-size: 40px;
        padding-bottom: 10px
    }
    
    .fa-circle {
        font-size: 10px;
        color: #EEEEEF
    }
    
    .green-dot {
        color: #4CAF50
    }
    
    .red-dot {
        color: #E64A19
    }
    
    .yellow-dot {
        color: #FFD54F
    }
    
    .grey-text {
        color: #BDBDBD
    }
    
    .green-text {
        color: #4CAF50
    }
    
    .block {
        border-right: 1px solid #F5EEEE;
        border-top: 1px solid #F5EEEE;
        border-bottom: 1px solid #F5EEEE
    }
    
    .profile-pic img {
        border-radius: 50%
    }
    
    .rating-dot {
        letter-spacing: 5px
    }
    
    .via {
        border-radius: 20px;
        height: 28px
    }
            </style>    
@endsection

@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
    
 
  
    $( "#target" ).submit(function(e) {
         e.preventDefault();
      $('#loader').show();
      $('#submit').attr('disabled','disabled');
     let email = $('#email').val();
     let pickup = $('#pickup').val();
     let destination = $('#destination').val();
     let describtion = $('#describtion').val();
     let company_id = $('#company_id').val();
     console.log(email);
     console.log(company_id);
     console.log(describtion);
     console.log(destination);
     console.log(pickup);
     
     $.ajaxSetup({
                headers: { }
            });
$.post('/api/requestquote',   // url
       {      email: email, 
              pickup: pickup, 
              destination: destination, 
              describtion: describtion, 
              company_id: company_id
                
                
       }, 
       function(data, status, jqXHR) {// success callback

        console.log(status);
        console.log(data);      
              
        if(data.code == "200"){
            $('#loader').hide();
           $('#submit').removeAttr('disabled');
           $('#message').show(); 
           $('#error').hide(); 
           $('#email').val() ="";
           $('#pickup').val() ="";
           $('#destination').val() ="";
           $('#describtion').val() ="";
           $('#company_id').val() ="";

        }else{
            $('#error').show();
           $('#message').hide(); 
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
        }


        }).fail(function(jqxhr, settings, ex) {
           $('#err').show();
           $('#loader').hide();
           $('#submit').removeAttr('disabled');
           alert("An Error Occured on the Server.")
         });

 
 
 
 
 });
 });
   </script>

@endsection