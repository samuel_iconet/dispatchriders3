<!DOCTYPE html>
<!--
Template Name: Materialize - Material Design Admin Template
Author: PixInvent
Website: http://www.pixinvent.com/
Contact: hello@pixinvent.com
Follow: www.twitter.com/pixinvents
Like: www.facebook.com/pixinvents
Purchase: https://themeforest.net/item/materialize-material-design-admin-template/11446068?ref=pixinvent
Renew Support: https://themeforest.net/item/materialize-material-design-admin-template/11446068?ref=pixinvent
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  
<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/vertical-modern-menu-template/dashboard-modern.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Jun 2020 01:40:49 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Dashboard Modern | Materialize - Material Design Admin Template</title>
    <link rel="apple-touch-icon" href="images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="/dash/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dash/css/pages/app-chat.min.css">
    <!-- BEGIN: VENDOR CSS-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/chartist-js/chartist-plugin-tooltip.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/dash/css/themes/vertical-modern-menu-template/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/css/themes/vertical-modern-menu-template/style.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="/dash/css/pages/intro.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/css/pages/user-profile-page.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/rateYo/jquery.rateyo.min.css">
   
    <link rel="stylesheet" type="text/css" href="/dash/css/pages/app-email-content.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/css/custom/custom.css">
   
    {{-- <link rel="stylesheet" type="text/css" href="/dash/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/dash/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: Page Level CSS--> --}}
    <!-- BEGIN: Custom CSS-->
    <style>
      .md-select-menu-container, 
      .md-open-menu-container {
          z-index:999999important;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="/dash/css/custom/custom.css">
    <link rel="stylesheet" href="/preload/css/normalize.css">
	<link rel="stylesheet" href="/preload/css/main.css">
	<style type="text/css">
	.back-link a {
		color: #4ca340;
		text-decoration: none; 
		border-bottom: 1px #4ca340 solid;
	}
	.back-link a:hover,
	.back-link a:focus {
		color: #408536; 
		text-decoration: none;
		border-bottom: 1px #408536 solid;
	}
	h1 {
		height: 100%;
		/* The html and body elements cannot have any padding or margin. */
		margin: 0;
		font-size: 14px;
		font-family: 'Open Sans', sans-serif;
		font-size: 32px;
		margin-bottom: 3px;
	}
	.entry-header {
		text-align: left;
		margin: 0 auto 50px auto;
		width: 80%;
        max-width: 978px;
		position: relative;
		z-index: 10001;
	}
	#demo-content {
		padding-top: 100px;
	}
	</style>
    <!-- END: Custom CSS-->
  </head>
  <!-- END: Head-->
  <body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">
    <div id="app"> 
    <!-- BEGIN: Header-->
    <header class="page-topbar" id="header">
      <div class="navbar navbar-fixed"> 
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
          <div class="nav-wrapper">
            
            <ul class="navbar-list right">
              <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
              <li class="hide-on-large-only search-input-wrapper"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
              <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">5</small></i></a></li>
              <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="/dash/images/avatar/avatar-7.png" alt="avatar"><i></i></span></a></li>
            </ul>
           
            <!-- notifications-dropdown-->
            <ul class="dropdown-content" id="notifications-dropdown">
              <li>
                <h6>NOTIFICATIONS<span class="new badge">5</span></h6>
              </li>
              <li class="divider"></li>
              <li><a class="black-text" href="#!"><span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>
                <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
              </li>
            
            </ul>
            <!-- profile-dropdown-->
            <ul class="dropdown-content" id="profile-dropdown">
              <li class="divider"></li>
              <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li>
              <li><a class="grey-text text-darken-1" href="user-login.html"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
          </div>
          <nav class="display-none search-sm">
            <div class="nav-wrapper">
              <form id="navbarForm">
                <div class="input-field search-input-sm">
                  <input class="search-box-sm mb-0" type="search" required="" id="search" placeholder="Explore Materialize" data-search="template-list">
                  <label class="label-icon" for="search"><i class="material-icons search-sm-icon">search</i></label><i class="material-icons search-sm-close">close</i>
                  <ul class="search-list collection search-list-sm display-none"></ul>
                </div>
              </form>
            </div>
          </nav>
        </nav>
      </div>
    </header>
    <!-- END: Header-->
   
    



    <!-- BEGIN: SideNav-->
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
      <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down" src="/dash/images/logo/materialize-logo-color.png" alt="materialize logo"/><img class="show-on-medium-and-down hide-on-med-and-up" src="/dash/images/logo/materialize-logo.png" alt="materialize logo"/><span class="logo-text hide-on-med-and-down">Materialize</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
      </div>
      <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class="active bold"><a href="/dashboard" class="active"  ><i class="material-icons">settings_input_svideo</i><span class="menu-title" >Dashboard</span></a>
      
        </li>
        
        
        <li class="active">  <a href="/support"><i class="material-icons">email</i><span data-i18n="Modern">Support Tickets</span></a>
        </li>
        <li class="active"><router-link to="/companylist"> <i class="material-icons">email</i><span data-i18n="Modern">Logistic Company List</span></router-link>
        </li>
        
        <li class="navigation-header"><a class="navigation-header-text">Finance</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold">
          <router-link to="/account"> 
            <a class="waves-effect waves-cyan " ><i class="material-icons">account_balance_wallet</i><span class="menu-title" >Account</span></a>

      </router-link>
        </li>
       
      </ul>
      <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    
    <div id="main" >
      <div class="row">
      
        
               
                <!-- //section is here -->
                <router-view></router-view>
    

       
      </div>
    </div>
    <!-- END: Page Main-->


    <!-- BEGIN: Footer-->

   
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    {{-- <script src="/dash/js/vendors.min.js"></script> --}}
    <script src="/dash/js/scripts/app-chat.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    {{-- <script src="/dash/vendors/chartjs/chart.min.js"></script>
    <script src="/dash/vendors/chartist-js/chartist.min.js"></script>
    <script src="/dash/vendors/chartist-js/chartist-plugin-tooltip.js"></script>
    <script src="/dash/vendors/chartist-js/chartist-plugin-fill-donut.min.js"></script> --}}
    
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="/dash/js/plugins.min.js"></script>
    <script src="/dash/js/search.min.js"></script>
    <script src="/dash/js/scripts/customizer.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/dash/vendors/sweetalert/sweetalert.css">
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="/preload/js/main.js"></script>
    {{-- <script src="/dash/vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="/dash/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="/dash/vendors/data-tables/js/dataTables.select.min.js"></script> --}}
<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
{{-- <script src="/dash/js/scripts/dashboard-modern.js"></script> --}}
<script src="/dash/js/scripts/extra-components-sweetalert.min.js"></script>
<script src="/dash/vendors/sweetalert/sweetalert.min.js"></script>
<script src="/dash/vendors/rateYo/jquery.rateyo.min.js"></script>
<script src="/dash/js/scripts/extra-components-ratings.min.js"></script>
<script src="/dash/js/scripts/app-email-content.min.js"></script>

    <!-- END PAGE LEVEL JS-->
  
  </body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/vertical-modern-menu-template/dashboard-modern.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Jun 2020 01:40:50 GMT -->
</html>