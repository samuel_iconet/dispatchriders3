<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from themes.startbootstrap.com/sb-ui-kit-pro/landing-directory.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Feb 2020 21:04:09 GMT -->
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content />
        <meta name="author" content />
        <title>Dispatch Riders</title>
        <link href="/css/styles.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="/assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="/cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="/cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .checked {
  color: orange;
}
        </style>
        @yield('style')
    </head>
    <body>
        <div id="layoutDefault">
            <div id="layoutDefault_content">
                <main>
                    <nav class="navbar navbar-marketing navbar-expand-lg bg-white navbar-light">
                        <div class="container">
                            <a class="navbar-brand text-dark" href="/index.html">Dispatch Riders</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i data-feather="menu"></i></button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto mr-lg-5">
                                    <li class="nav-item"><a class="nav-link" href="/">Home </a></li>
                                    <li class="nav-item dropdown dropdown-xl no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownDemos" href="/about" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us<i class="fas fa-chevron-right dropdown-arrow"></i></a>
                                    </li>
                                    <li class="nav-item dropdown dropdown-lg no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownPages" href="/contact" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contact Us<i class="fas fa-chevron-right dropdown-arrow"></i></a>
                                    </li>
                                    <li class="nav-item dropdown no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownDocs" href="/#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Market Place<i class="fas fa-chevron-right dropdown-arrow"></i></a>
                                     </li>
                                      <li class="nav-item dropdown no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownDocs" href="/login" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login<i class="fas fa-chevron-right dropdown-arrow"></i></a>
                                     </li>
                                </ul>
                                <a class="btn-teal btn rounded-pill px-4 ml-lg-4" href="/company_register">Register Your Company Now<i class="fas fa-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </nav>
               @yield('body')
                </main>
            </div>
            <div id="layoutDefault_footer">
                <footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="footer-brand">Dispatch Riders</div>
                                <div class="mb-3">Your Number one stop for Logistics Service</div>
                                <div class="icon-list-social mb-5">
                                    <a class="icon-list-social-link" href="/javascript:void(0);"><i class="fab fa-instagram"></i></a><a class="icon-list-social-link" href="/javascript:void(0);"><i class="fab fa-facebook"></i></a><a class="icon-list-social-link" href="/javascript:void(0);"><i class="fab fa-github"></i></a><a class="icon-list-social-link" href="/javascript:void(0);"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Product</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="/javascript:void(0);">Landing</a></li>
                                            
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Technical</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="/javascript:void(0);">Documentation</a></li>
                                           
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Includes</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="/javascript:void(0);">Utilities</a></li>
                                        
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="text-uppercase-expanded text-xs mb-4">Legal</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="/javascript:void(0);">Privacy Policy</a></li>
                                            <li class="mb-2"><a href="/javascript:void(0);">Terms and Conditions</a></li>
                                            <li><a href="/javascript:void(0);">License</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-5" />
                        <div class="row align-items-center">
                            <div class="col-md-6 small">Copyright &#xA9; Your Website 2020</div>
                            <div class="col-md-6 text-md-right small">
                                <a href="/javascript:void(0);">Privacy Policy</a>
                                &#xB7;
                                <a href="/javascript:void(0);">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="/code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="/stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="/js/scripts.js"></script>
            @yield('script')
        <sb-customizer project="sb-ui-kit-pro"></sb-customizer>
    </body>

<!-- Mirrored from themes.startbootstrap.com/sb-ui-kit-pro/landing-directory.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Feb 2020 21:04:10 GMT -->
</html>
