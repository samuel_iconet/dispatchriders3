<?php

namespace App;
use App\company_subscribtion;
use App\rider_request;
use App\company;

class companyRanking 
{
    public function rankCompanies($companies)
    {
        foreach($companies as $company){
         $points = 0;
         $subpoint = 0;
         $requestpoints  = 0;
         $rating_point = 0;
         
         $total_requests  = count(rider_request::all());
         $total_subs = count(company_subscribtion::all());
         $company_rating = $this->getRating($company);
         $company_request = $company->riderrequests()->count();
         $company_subs = $company->subscribers()->count();
        if($total_requests != 0){
            $requestpoints = (($company_request / $total_requests) * 40);
        }
        if($total_subs != 0){
            $subpoint = (($company_subs / $total_subs) * 20); 
        }
        if($company_rating != 0){
            $rating_point = (($company_rating / 5) * 40); 
        }
        $points = $subpoint + $requestpoints + $rating_point;
        $company['subpoints'] = $subpoint;
        $company['points'] = round($points, 1);
        $company['rating_points'] = $rating_point;
        $company['requestpoints'] = $requestpoints;
        $company['rating_percentage'] = ($company_rating / 5) *100;

        }
        return $companies;
    }
    public function getRating($company){
        $reviews = $company->review()->where('status' , '1')->get();
        $rating = 0;
        $rating_count = count($reviews) ;
      if($rating_count  > 0){
        foreach($reviews as $review){
          $rating += $review->rating;
            
        }
        return $rating/$rating_count;
      }else {
          return 0;
      }

    }
}