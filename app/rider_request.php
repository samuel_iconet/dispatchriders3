<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rider_request extends Model
{
    //
    public function company()
    {
        return $this->belongsTo('App\company');
    }
    public function route()
    {
        return $this->belongsTo('App\route');
    }
    public function rider()
    {
        return $this->belongsTo('App\driver' , 'rider_id' , 'user_id');
    }
}
