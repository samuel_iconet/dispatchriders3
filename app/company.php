<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    //
    public function review()
    {
        return $this->hasMany('App\review');
    }
    public function routes()
    {
        return $this->hasMany('App\route');
    }
    public function subscribers()
    {
        return $this->hasMany('App\company_subscribtion');
    }
    public function riderrequests()
    {
        return $this->hasMany('App\rider_request');
    }
}
