<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\support_ticket;
use App\supportManagement;
use App\support_message;
class supportticketcontroller extends Controller
{
     
      public function __construct()
      {
          $this->middleware('auth:api');
  
      }
      public function getticket($ticket_id){
        $data = support_ticket::where(['user_id' => Auth::user()->id , 'id' => $ticket_id] )->first();
        if(isset($data)){
          $ticket = support_ticket::findOrFail($ticket_id);
          $supportManagement = new supportManagement();
          $supportManagement->clearUread($ticket);
          $response['messages'] =$ticket->messages()->get();
          $response['code'] = 200;
          return response()->json( $response,200);
        }else{
          $response['code'] = 404;
          return response()->json( $response,200); 
        }
    
      }
      public function gettickets(){
       
        $tickets = support_ticket::where('user_id', Auth::user()->id)->get();
         $supportManagement = new supportManagement();
        foreach($tickets as $ticket){
         $ticket['data'] = $supportManagement->populateUserTicket($ticket);
        }
        $sub_companies =  Auth()->User()->companies_subto()->get();
        if(count($sub_companies) > 0){
          foreach($sub_companies as $company){
            $company['data'] = $company->company()->first();
          }

        }
        $response['sub_companies'] = $sub_companies;
        $response['user'] = Auth::user();
         $response['tickets']   = $tickets;
         $response['code'] = 200;
         return response()->json( $response,200);
      }
      public function createticket(request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'title' => 'required',
            'message' => 'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      
      if($request->type == 'general'){
        $ticket = new support_ticket;
        $ticket->ticket_type = $request->type;
        $ticket->title = $request->title;
        $ticket->user_id = Auth::user()->id;
        $ticket->status = 'pending';
        $ticket->save();


      }else {
        $ticket = new support_ticket;
        $ticket->ticket_type = $request->type;
        $ticket->user_id = Auth::user()->id;
        $ticket->title = $request->title;
        $ticket->status = 'pending';
        $ticket->company_id = $request->company_id;
        $ticket->save();
      }

      $message = new  support_message;
      $message->support_ticket_id = $ticket->id;
      $message->message = $request->message;
      $message->type = 'forward';
      $message->save();
      $response['code'] = 200;
      return response()->json( $response,200);
      }

 public function replyticket(request $request){
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required',
            'message' => 'required',
      ]);
      if ($validator->fails()) {

        return $validator->messages();
   } 
   $message = new  support_message;
      $message->support_ticket_id = $request->ticket_id;
      $message->message = $request->message;
      $message->type = 'forward';
      $message->save();
      $response['code'] = 200;
      return response()->json( $response,200);
      }
 public function endticket($ticket_id){
    $ticket = support_ticket::where(['user_id' => Auth::User()->id , 'id' => $ticket_id])->first();
    if(isset($ticket)){
        $ticket->status = 'completed';
        $ticket->save();
        $response['code'] = 200;
        return response()->json( $response,200);
    }else{
        $response['code'] = 404;
        $response['error'] = "Invalid Ticket";
        return response()->json( $response,200);
    }
 }
}
