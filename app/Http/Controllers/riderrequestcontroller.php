<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class riderrequestcontroller extends Controller
{
    //
    public function __construct()
      {
          $this->middleware('auth:api');
  
      }
    public function getdetails(){
        $sub_companies =  Auth()->User()->companies_subto()->get();
        if(count($sub_companies) > 0){
          foreach($sub_companies as $company){
            $company['data'] = $company->company()->first();
            if(isset( $company['data'])){
                $company['routes'] = $company['data']->routes()->get();

            }else{
                $company['routes'] = null;
            }
            
          }
        }

        $response['sub_companies'] =  $sub_companies;
        $response['code'] =  200;
        return response()->json( $response,200);
    }
}
