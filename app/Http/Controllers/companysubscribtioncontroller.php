<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\companyRanking;
use App\company;
use App\company_subscribtion;
use Auth;
class companysubscribtioncontroller extends Controller
{
    //
    public function __construct()
      {
          $this->middleware('auth:api');
  
      }
      public function getcompanies(){
       $companies = company::where('status' , '1')->get();
       $sub_companies =  Auth()->User()->companies_subto()->get();
       $companyRanking  = new companyRanking;
       $rankedCompanies =  $companyRanking->rankCompanies($companies);
       $unsortedCompanies = collect($rankedCompanies);
       $sortedCompanies =  $unsortedCompanies->sortByDesc('points');
       $sortedCompanies = $sortedCompanies->values()->all();
       foreach($sortedCompanies as $company){
        $company->status = '0'; 
       }
       if(count($sub_companies) > 0){
           foreach($sub_companies as $sub){
            foreach($sortedCompanies as $company){
                if($sub->company_id == $company->id){
                    $company['subscribe'] = '1';
                    $company->status = '1';
                }
            }
           }
       }else{
        foreach($sortedCompanies as $company){ 
            $company['subscribe'] = '0';
            $company->status = '0';
        }      
    }
    $response['companies'] = $sortedCompanies;
     $response['code'] = 200;
      return response()->json( $response,200);
      }
      public function subscribeto($company_id){
      
      
        $issub = company_subscribtion::where(['user_id'=> Auth::user()->id , 'company_id' => $company_id])->first();
        if(isset($issub)){
            $response['error'] = 'Already Subscribed to this company';
            $response['code'] = 404;
         return response()->json( $response,200);
        }else{
            $sub = new company_subscribtion;
            $sub->company_id = $company_id;
            $sub->user_id = Auth::user()->id;
            $sub->save();
            $response['code'] = 200;
      return response()->json( $response,200);   
          
        }
      }
      public function unsubscribeFrom($company_id){
          $issub = company_subscribtion::where(['user_id'=> Auth::user()->id , 'company_id' => $company_id])->first();
        if(isset($issub)){
            $issub->delete();
            $response['code'] = 200;
      return response()->json( $response,200);
        }else{
            $response['error'] = 'You Are not Subscribed to this company';
            $response['code'] = 404;
      return response()->json( $response,200);
        }
      }
}
