<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\company;
use App\review;
use App\general_review;
use App\quoteRequest;
use Validator;
class homecontroller extends Controller
{
    //
    public function index(){
      return view('backend');

    }
    public function requestquote(request $request){
      $validator = Validator::make($request->all(), [
        'email' => 'required',
        'pickup' => 'required', 
        'destination' => 'required', 
        'describtion' =>  'required', 
        'company_id' => 'required'
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
  $quoterequest = new quoteRequest;
  $quoterequest->email = $request->email;
  $quoterequest->pickup = $request->pickup;
  $quoterequest->destination = $request->destination;
  $quoterequest->describtion = $request->describtion;
  $quoterequest->company_id = $request->company_id;
  $quoterequest->status = 'pending';
  $quoterequest->save();

 $result['code'] = 200;
 return response()->json( $result,200);

    }
    public function company_detrails($company_id){
        $company = company::findOrFail($company_id);
        $company = $this->getReviews($company);
        $routes= $company->routes()->where('status' , '1')->get();
        return view('companydetails' ,
        ['company' => $company ,
         'routes' => $routes 
         ]);
    }
    public function home(){
     $companies = company::where('status' , '1')->get();
     $latestCompanies = company::where('status' , '1')->orderBy('created_at', 'asc')->take(3)->get();
     foreach($latestCompanies as $company){
        $company = $this->getReviews($company);
        } 
    foreach($companies as $company){
    $company = $this->getReviews($company);
    }
     $reviews = general_review::all();
     return view('home' ,
      ['companies' => $companies ,
       'review' => $reviews , 
       'latestcompanies' => $latestCompanies, 
       'reviews' =>$reviews
       ]);
    }
    public function calRating($reviews){
        $rating = 0;
        $rating_count = count($reviews) ;
      if($rating_count  > 0){
        foreach($reviews as $review){
          $rating += $review->rating;
            
        }
        return round($rating/$rating_count, 1);
      }else {
          return 0;
      }
    }
    public function getReviews($company){
        $reviews = $company->review()->where('status' , '1')->get();
        $rating = $this->calRating($reviews);
        $rating_rounded =  floor($rating);
        foreach($reviews as $review){
          $review['user']  = $review->user;  
        }
        $company['reviews'] = $reviews;
        $company['rating'] = $rating;
        $company['rating_rounded'] = $rating_rounded;
        return $company;
    }
}
