<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\account;
use App\accountManagement;
use Auth;
class useraccountcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function getAccount(){
        $account = account::where(['holder_type' => 'user' ,  'holder_id' => Auth::user()->id])->first();
        if(isset($account)){
          $transactions = $account->transactions();
          $response['account'] = $account;
          $response['transactions'] = $transactions;
          $response['code'] = 202;
          return response()->json( $response,200); 
        }else{
            $response['code'] = 404;
            return response()->json( $response,200);
        }
    }
    public function withdrawFunds(request $request){
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            
      ]);
    
      if ($validator->fails()) {
           return $validator->messages();
      }
      
    }
    public function transferFund(request $request){
        $validator = Validator::make($request->all(), [
            'user_email' => 'required',
            'amount' => 'required',
            
      ]);
    
      if ($validator->fails()) {
    
           return $validator->messages();
      }
      $accountManagement = new accountManagement;
      $res = $accountManagement->transfer($request);
      if($res == 'success'){
        $response['code'] = 200;
            return response()->json( $response,200);
      }
      else if($res == 'invalid_user'){
        $response['code'] = 404;
        $response['error'] = "User Email Invalid";
        return response()->json( $response,200);
      }else if($res == 'limit'){
        $response['code'] = 500;
        $response['error'] = "Insuficient Balance";
        return response()->json( $response,200); 
      }
    }
    
    public function cancelRequest(){
    
    }
}
