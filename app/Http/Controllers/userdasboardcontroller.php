<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\accountManagement;
use App\account;
use App\User;
use App\rider_request;
use App\requestManagement;
use App\review;
class userdasboardcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
   public function home(){
     $account = account::where(['holder_id' => Auth::user()->id , 'holder_type' => 'user'])->first();
    if(!isset($account)){
     $accountManagement = new accountManagement;
     $naccount = $accountManagement->createAccount(Auth::user()->id , 'user');
     $response['account'] = $naccount; 
    }else{
        $response['account'] = $account; 

    }
     
    $response['user'] = Auth::user();;  
    $response['active_request_count'] = rider_request::where('user_id' , Auth::user()->id )->whereNotIn('status', [9, 8])->count();
    $response['inactive_request_count'] = rider_request::where('user_id' , Auth::user()->id )->whereIn('status', [9, 8])->count();
    $requests  = rider_request::where('user_id' , Auth::user()->id  )->orderBy('status' ,'asc')->get();
    $requestManagement = new requestManagement;
    $response['requests'] = $requestManagement->generateProcess($requests);

    $response['code'] = 200;
    return response()->json( $response,200);
   } 
   public function cancelRequest($request_id){

    $request =  rider_request::where(['id' => $request_id , 'user_id' => Auth::user()->id , 'status' => '1'])->first();
    if(isset($request)){
    $request->status = '8';
    $request->save();
    $response['code'] = 200;
    return response()->json( $response,200);
    }else{
    $response['code'] = 404;
    return response()->json( $response,200);    
    }
    

   }
   public function confirmRequest(request $request){
    $validator = Validator::make($request->all(), [
        'request_id' => 'required',
        'rating' => 'required',
        'review' => 'required',
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
    $rider_request =  rider_request::where(['id' => $request->request_id , 'user_id' => Auth::user()->id , ['status' , '<>' , '8']])->first();
    if(isset($rider_request)){
    $company_id = $rider_request->company_id;
    $review = new review;
    $review->company_id = $company_id;
    $review->rating = $request->rating;
    $review->review = $request->review;
    $review->status = '1';
    $review->user_id = Auth::user()->id;
    $review->save();
    $rider_request->status = '9';
    $rider_request->save();
    $response['code'] = 200;
    return response()->json( $response,200);
    }else{
    $response['code'] = 404;
    return response()->json( $response,200);    
    }
    

   }
}
