<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Illuminate\Support\Facades\Auth;
class usermanagementcontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function editUser(request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'contact' => 'required',
            'address' => 'required',
            'profile' => 'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $user = User::find(Auth::user()->id);
      $user->name = $request->name;
      $user->contact = $request->contact;
      $user->address = $request->address;
      if ($request->has('profile')) {
        $image = $request->file('profile');
        $name = 'profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->profile->storeAs('profile', $filePath, 'public');
        $profile = $filePath;

    }
    $user->profile = $profile;
    $user->save();
    $res['code'] = '200';
    return response()->json($res ,200);
    }
    public function editPassword(request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
         
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 

      if( $check  = Auth::guard('web')->attempt([
        'email' => Auth::user()->email,
        'password' => $request['old_password']
    ])){
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->new_password);
        $user->save();
        $res['code'] = '200';
    return response()->json($res ,200);

      }else{
        $res['code'] = '404';
        $res['error'] = 'invalid password';
        return response()->json($res ,200);
      }
    }
}
