<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use  Str;
use Validator;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use App\subscriber;
use App\User;
use App\company;
use App\company_document;

class authcontroller extends Controller
{
    //
    public function register(request $request){
        $validator = Validator::make($request->all(), [
            "username" =>  "required",
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
    
   
                $user = new User;
                $user->status = 0;
                $user->name = $request->username;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->remember_token = Str::random(20);
                $user->save();


                $response['code'] = 200;
                $response['user'] = $user;
                Mail::to($user)->send(new WelcomeMail($user));
                return response()->json($response ,200);
    }
    public function confirm($user_id , $remember){
        $user = User::findOrFail($user_id);
        if($user->status == '0' && $remember == $user->remember_token){
            $user->status = '1';
            $user->remember_token = "";
            $user->save();
            return view('login' , ['message' =>  "Email Verification Successful, please login into your account now!!!"]);
            
        }
        return view('login' , ['error' => "Email Verification Fail Request new Verification Email."]);
    }

    public function login(request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      
    $client =  new Client([
          'base_uri' => url()->full(),
        'headers' => [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ],
    ]);
        $result = $client->get('/oauth/token', [
       'form_params' => [
        "grant_type" =>  "password",
        "client_id" =>  "2",
        "client_secret" =>  "A0faLcaxXb3uBDdoWUjNX7LYOorOgmNN4WBJfgxA",
        "username" =>  $request->email,
        "password" =>  $request->password,
        "scope" => "*"
    ]
]);
    $res['body'] = $result ;
   
    return response()->json($res ,200);
    }
    public function subscribe(request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:subscribers',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }

      $subscriber = new subscriber;
      $subscriber->email = $request->email;
      $subscriber->save();
      $res['message'] = "Subscription Successful.";
      $res['code'] = '200';
      return response()->json($res ,200);
    }
    public function company_register(request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'company_email' => 'required',
            'company_contact' => 'required',
            'company_address' => 'required',
            'company_moto' => 'required',
            'company_logo' => 'required',
            'company_cac' => 'required',
            'company_licence' => 'required',
            'company_bank' => 'required',
            'company_accnum' => 'required',
            'company_accname' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
             'state' => "required"
      ] ,);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $company = company::where('email' , $request->company_email)->first();
      if(isset($company)){
        $res['message'] = "Company Email Already Used.";
        $res['code'] = '404';
        return response()->json($res ,200);
      } 
      if ($request->has('company_logo')) {
        $image = $request->file('company_logo');
        $name = 'companylogo'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->company_logo->storeAs('logo', $filePath, 'public');
        $company_logo = $filePath;

    }
    $company = new company;
    $company->name = $request->company_name;
    $company->email = $request->company_email;
    $company->state_id = $request->state;
    $company->status = '0';
    $company->address = $request->company_address;
    $company->contact = $request->company_contact;
    $company->moto = $request->company_moto;
    $company->logo = $company_logo;

    $company->save();
    if ($request->has('company_cac')) {
        $image = $request->file('company_cac');
        $name = 'companycac'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->company_cac->storeAs('cac', $filePath, 'public');
        $companycac = $filePath;
        $doc = new company_document;
        $doc->doc_name = 'company_cac';
        $doc->file_name = $companycac;
        $doc->company_id = $company->id;
        $doc->save();

    }
    if ($request->has('company_licence')) {
        $image = $request->file('company_licence');
        $name = 'companylicence'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->company_licence->storeAs('licence', $filePath, 'public');
        $companylicence = $filePath;
        $doc = new company_document;
        $doc->doc_name = 'company_licence';
        $doc->file_name = $companycac;
        $doc->company_id = $company->id;
        $doc->save();
    }
    $user = new User;
    $user->status = 0;
    $user->name = $request->fname .' '. $request->lname;
    $user->email = $request->email;
    $user->password = bcrypt($request->password);
    $user->remember_token = Str::random(20);
    $user->save();
    Mail::to($user)->send(new WelcomeMail($user));
    $res['message'] = "Registration Successful.";
    $res['code'] = '200';
    return response()->json($res ,200);
    }
}
