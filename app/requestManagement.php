<?php

namespace App;
use App\account;
use App\rider_request;
use App\request_process;
class requestManagement 
{
    //
    public function pendingCount($user_id){
    //  $request = request::where(['user_id' =>$user_id ,  '<>'])
    }
    public function completeCount($user_id){
        
    }
    public function generateProcess($requests){
        foreach($requests as $request){
            $process = request_process::find($request->status);
            $request['company'] = $request->company()->first();
            $request['route'] = $request->route()->first();
            $rider = $request->rider()->first();
            $request['rider_user'] = $rider->user()->first();
            if(isset($process)){
                
                $request['process'] = $process->process_name;
            }else{
                $request['process'] = '';
            }
        }
        return $requests;
    }

}
