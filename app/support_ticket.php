<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  Carbon\Carbon;
class support_ticket extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function company()
    {
        return $this->belongsTo('App\company');
    }
    public function messages()
    {
        return $this->hasMany('App\support_message');
    }
    public function getCreatedAtAttribute($value)
{
    return  Carbon::parse($value)->format('d/m/Y');;
}

}
