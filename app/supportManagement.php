<?php

namespace App;
use App\support_ticket;
use App\support_message;
class supportManagement 
{
  public function populateUserTicket($ticket){
    $data['messages'] = $ticket->messages()->get();
   $data['unread']  = $data['messages']->where(['type' => 'backward' , 'view' => '0'])->count();
    $data['user'] = $ticket->user()->first();
    if($ticket->type != "general"){
    $data['company'] = $ticket->company()->first();
    }
    return $data;
  }  
  public function populateAdminTicket($ticket){
    $data['messages'] = $ticket->messages();
   $data['unread']  = $data['messages']->where(['type' => 'forward' , 'view' => '0'])->count();
    $data['user'] = $ticket->user();
    if($ticket->type != "general"){
    $data['company'] = $ticket->company();
    }
    return $data;
  } 
  public function clearUread($ticket){
    $data = $ticket->messages();
    if(isset($data) ){
      $data = $data->where(['type' => 'backward' , 'view' => '0'])->get();
      foreach($data as $message){
          $message->view = '1';
          $message->save();
      }
    }
   
    return true;
  } 
  public function populateMessages($ticket){
   
  }


}
