<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driver extends Model
{
    //
    public function company()
    {
        return $this->belongsTo('App\route');
    }
    public function user()
    {
        return $this->belongsTo('App\user');
    }
}
